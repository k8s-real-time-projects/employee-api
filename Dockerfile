# Use the official Python image as the base image
FROM python:3.8-slim-buster

# Set environment variables for Flask
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

# Set the working directory inside the container
WORKDIR /app

# Copy the current directory contents into the container
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install -f --no-cache-dir -r requirements.txt

# Expose the port your Flask app will run on
EXPOSE 5000

# Start the Flask application
CMD ["flask", "run"]
